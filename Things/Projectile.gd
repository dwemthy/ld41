extends KinematicBody2D

export(PackedScene) var Explosion
export var speed = 10
export var damage = 1
export var limit = -1

var direction
var travelled = 0

func set_direction(dir):
	direction = dir
	var rotation = atan2(direction.x, -direction.y)
	rotate(rotation)

func _physics_process(delta):
	if limit > 0 && travelled >= limit:
		die()
	
	var collision = move_and_collide(direction * speed * delta)
	
	if collision != null:
		hit(collision.collider)
	else:
		travelled += speed * delta

func hit(body):
	body.emit_signal("hit", damage)
	die()
	
func die():
	if Explosion != null:
		var explosion = Explosion.instance()
		get_parent().add_child(explosion)
		explosion.position = position
		
	queue_free()