extends Area2D

export var value = 5
export var collectSpeed = 256

var elephant

func _ready():
	connect("body_entered", self, "collect")

func collect(body):
	elephant = body
	
func _physics_process(delta):
	if elephant != null:
		var toElephant = elephant.position - position
		
		if toElephant.length() < collectSpeed * 0.25:
			elephant.emit_signal("collect", value)
			queue_free()
		else:
			toElephant = toElephant.normalized()
			position = position + (toElephant * collectSpeed * delta)