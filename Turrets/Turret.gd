extends Area2D

export var fireCooldown = 1.0
export var fireRange = 200

export var current_level = 0
export var upgrade_cost = 25
export(PackedScene) var Projectile
export var projectile_count = 1
export var spread = 10
export(PackedScene) var nextLevel
export(Texture) var normal
export(Texture) var hover
export(Texture) var pressed

var targets = []
var fireElapsed
var shape

func _ready():
	connect("body_entered", self, "acquire")
	connect("body_exited", self, "unacquire")
	connect("area_entered", self, "acquire")
	connect("area_exited", self, "unacquire")
	fireElapsed = fireCooldown
	
	var shape = get_node("CollisionShape2D")
	var circle = CircleShape2D.new()
	circle.radius = fireRange
	shape.set_shape(circle)
	
func _process(delta):
	if fireElapsed < fireCooldown:
		fireElapsed += delta
		
	if targets.size() > 0:
		focus_target()
	
func current_level():
	return current_level

func at_max_level():
	return nextLevel == null
	
func upgrade_cost():
	return upgrade_cost
	
func acquire(body):
	targets.append(body)
	
func global_pos():
	return to_global(Vector2())

func unacquire(body):
	var to_remove = -1
	for i in range(0, targets.size()):
		if targets[i] == body:
			to_remove = i
			break
	
	if to_remove >= 0:
		targets.remove(to_remove)

func can_fire():
	return fireElapsed >= fireCooldown

func fire(direction):
	fireElapsed = 0.0
	if projectile_count == 1:
		var projectile = Projectile.instance()
		projectile.set_direction(direction.normalized())
		projectile.position = global_pos()
		get_node("/root/World").add_child(projectile)
	else:
		var aim_angle = atan2(direction.y, direction.x)
		var start = aim_angle - (deg2rad(spread) * 0.5)
		var step = deg2rad(spread) / (projectile_count - 1)
		for i in range(0, projectile_count):
			var step_direction = Vector2(cos(start), sin(start))
			
			var projectile = Projectile.instance()
			projectile.set_direction(step_direction.normalized())
			projectile.position = global_pos()
			get_node("/root/World").add_child(projectile)
			
			start += step
	
func focus_target():
	var target = targets[0]
		
	if target != null && target.get_parent() != null:
		var dist = target.position - position
		for i in range(1, targets.size()):
			var alt_dist = targets[i].position - position
			if alt_dist.length() > dist.length():
				target = targets[i]
				dist = alt_dist
			
		var toTarget = target.position - global_pos()
		var targetAngle = atan2(toTarget.x, -toTarget.y)
		rotate(targetAngle - rotation - get_node("/root/World/Elephante").rotation)
		
		if can_fire():
			fire(toTarget)
