extends TextureRect

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	add_to_group("lose")
	
func lose():
	show()
	get_tree().paused = true