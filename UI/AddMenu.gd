extends HBoxContainer

export(PackedScene) var Archer
export var archer_cost = 25
export(PackedScene) var Bomber
export var bomber_cost = 50
export(PackedScene) var Wizard
export var wizard_cost = 100
export(PackedScene) var Robot
export var robot_cost = 75

var robot
var wizard
var bomber
var archer

var slot_group

func _ready():
	robot = get_node("Robot")
	robot.get_node("TextureButton").connect("button_down", self, "add_robot")
	wizard = get_node("Wizard")
	wizard.get_node("TextureButton").connect("button_down", self, "add_wizard")
	bomber = get_node("Bomber")
	bomber.get_node("TextureButton").connect("button_down", self, "add_bomber")
	archer = get_node("Archer")
	archer.get_node("TextureButton").connect("button_down", self, "add_archer")
	
	add_to_group("dust_watchers")
	
	hide()
	
func update_dust(dust_update):
	archer.get_node("TextureButton").disabled = dust_update < archer_cost
	robot.get_node("TextureButton").disabled = dust_update < robot_cost
	bomber.get_node("TextureButton").disabled = dust_update < bomber_cost
	wizard.get_node("TextureButton").disabled = dust_update < wizard_cost
	
func set_slot_group(slot_group_name):
	slot_group = slot_group_name
	
func add_archer():
	add_turret(Archer.instance(), archer_cost)
	
func add_bomber():
	add_turret(Bomber.instance(), bomber_cost)
	
func add_wizard():
	add_turret(Wizard.instance(), wizard_cost)
	
func add_robot():
	add_turret(Robot.instance(), robot_cost)
	
func add_turret(turret, cost):
	get_tree().call_group("dust_spent", "spend_dust", cost)
	get_tree().call_group(slot_group, "set_turret", turret)
	