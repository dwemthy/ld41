extends HBoxContainer

var progress

func _ready():
	progress = get_node("TextureProgress")
	add_to_group("health_monitor")
	
func set_health(health):
	progress.value = health
	
func set_health_max(health_max):
	progress.max_value = health_max