extends Label

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	add_to_group("dust_watchers")

func update_dust(amount):
	text = String(amount)