extends HBoxContainer

var progress

func _ready():
	progress = get_node("TextureProgress")
	add_to_group("stamina_monitor")
	
func set_stamina(stamina):
	progress.value = stamina
	
func set_stamina_max(stamina_max):
	progress.max_value = stamina_max