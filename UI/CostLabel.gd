extends Label

func set_cost(cost):
	if cost > 0:
		text = String(cost)
	elif cost == -1:
		text = "-"
	else:
		text = ""