extends NinePatchRect

export var upgrade_group = "cannon1"
export(Texture) var normal
export(Texture) var hover
export(Texture) var pressed
export var add = true

var cost
var label
var button
var level
var add_menu

func _ready():
	add_menu = get_node("../AddMenu")
	if add_menu != null:
		add_menu.set_slot_group(upgrade_group)
	
	label = get_node("UpgradeCost")
	level = get_node("CurrentLevel")
	
	button = get_node("UpgradeButton")
	button.connect("button_down", self, "clicked")
	set_textures(normal, hover, pressed)
	
	add_to_group("dust_watchers")
	add_to_group(upgrade_group + "_watchers")
	
	#request update signals
	get_tree().call_group(upgrade_group, "refresh")
	get_tree().call_group("dust_spent", "spend_dust", 0)
	
func turret_set():
	add = false
	if add_menu != null:
		add_menu.hide()
		
func clicked():
	if add:
		if add_menu != null:
			if add_menu.visible:
				add_menu.hide()
			else:
				add_menu.show()
	else:
		get_tree().call_group(upgrade_group, "upgrade", upgrade_group)
	
func set_upgrade_cost(new_cost):
	cost = new_cost
	label.set_cost(cost)
	
	if cost < 0:
		button.disabled = true
	
func update_dust(dust_amount):
	if add:
		button.disabled = false
	elif cost != null && cost > 0 && dust_amount >= cost:
		button.disabled = false
		label.add_color_override("font_color", Color(1.0, 1.0, 1.0, 1.0))
	else:
		button.disabled = true
		label.add_color_override("font_color", Color(1.0, 0.0, 0.0, 1.0))
	
func update_level(level_value):
	level.text = String(level_value)

func set_textures(normal_tex, hover_tex, pressed_tex):
	button.texture_normal = normal_tex
	button.texture_hover = hover_tex
	button.texture_pressed = pressed_tex
	