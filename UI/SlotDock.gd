extends VBoxContainer

export(PackedScene) var SlotButton

var slots = {}

func _ready():
	add_to_group("add_slot")
	get_tree().call_group("platform", "refresh")
	
func add_slot(group_name):
	if !slots.has(group_name):
		var slot = SlotButton.instance()
		slot.get_node("UpgradeFrame").upgrade_group = group_name
		slots[group_name] = slot
		add_child(slot)
		move_child(slot, 0)