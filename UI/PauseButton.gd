extends TextureButton

var paused = false
var panel

func _ready():
	connect("button_down", self, "toggle_pause")
	panel = get_node("../../../PausePanel")
	panel.hide()
	
func toggle_pause():
	if paused:
		paused = false
		panel.hide()
	else:
		paused = true
		panel.show()

	get_tree().paused = paused