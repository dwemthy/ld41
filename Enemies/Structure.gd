extends KinematicBody2D

signal hit

export(PackedScene) var Dust
export var dust_drop = 1
export var health = 1

func _ready():
	connect("hit", self, "damage")

func damage(amount):
	health -= amount
	if health <= 0:
		spawn_dust()
		queue_free()
		
func spawn_dust():
	for i in range(0, dust_drop):
		var dust = Dust.instance()
		dust.position = position + Vector2(i * 5, i * 3)
		get_parent().add_child(dust)