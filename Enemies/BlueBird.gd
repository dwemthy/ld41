extends Area2D

signal hit

export(PackedScene) var Dust
export var damage = 1
export var health = 1
export var speed = 100
export var dust_drop = 1
export var avoid_duration = 2
export var roam_distance = 800

var elephant
var avoid_remaining = 0
var direction
var attacking = false
var bird_house

func _ready():
	connect("hit", self, "damage")
	connect("body_entered", self, "attack")
	connect("body_exited", self, "avoid")
	elephant = get_node("/root/World/Elephante")
	avoid_remaining = 0
	
func set_house(house):
	bird_house = house
	
func elephant_within_roam():
	if bird_house != null:
		if elephant != null:
			var house_to_elephant = bird_house.position - elephant.position
			var bird_to_elephant = position - elephant.position
			var house_to_bird = position - bird_house.position
			return (bird_to_elephant.length() <= roam_distance && house_to_bird.length() <= roam_distance) || house_to_elephant.length() <= roam_distance
		else:
			return false
	else:
		if elephant != null:
			var to_elephant = elephant.position - position
			return to_elephant.length() <= roam_distance
		else:
			return false
		
func _physics_process(delta):
	if elephant_within_roam():
		var toElephant = elephant.position - position
		toElephant = toElephant.normalized()
		
		if attacking:
			pass
		elif avoid_remaining > 0:
			avoid_remaining -= delta
			direction = -toElephant
		else:
			direction = toElephant
			
		if direction == null:
			return
			
		position += direction * speed * delta
		var rotate_by = atan2(direction.x, -direction.y)
		rotate_by -= rotation
		rotate(rotate_by)
	elif bird_house != null:
		var to_house = bird_house.position - position
		if to_house.length() < (roam_distance * 0.5):
			if to_house.length() == 0:
				var direction = Vector2(rand_range(-1.0, 1.0), rand_range(-1.0, 1.0))
				position += direction.normalized() * speed * delta
				var rotate_by = atan2(direction.x, -direction.y)
				rotate_by -= rotation
				rotate(rotate_by)
			else:
				var away_from_house = -to_house.normalized()
				position += away_from_house * speed * delta
				to_house = position - bird_house.position
				var rotate_by = atan2(away_from_house.x, -away_from_house.y)
				rotate_by -= rotation
				rotate(rotate_by)
		else:
			#patrol radius
			var away_from_house = -to_house.normalized()
			var away_angle = atan2(away_from_house.x, -away_from_house.y)
			var patrol = Vector2(cos(away_angle), sin(away_angle))
			position += patrol * speed * delta
			var rotate_by = atan2(patrol.x, -patrol.y)
			rotate_by -= rotation
			rotate(rotate_by)
	else:
		#roost?
		pass

func damage(amount):
	health -= amount
	if health <= 0:
		spawn_dust()
		if bird_house != null:
			bird_house.bird_died(self)
			
		queue_free()
		
func spawn_dust():
	for i in range(0, dust_drop):
		var dust = Dust.instance()
		dust.position = position + Vector2(i * 5, i * 3)
		get_parent().add_child(dust)
	
func attack(body):
	if body.collision_layer == 1:
		attacking = true
		body.damage(damage)
	else:
		body.hit(self)
	
func avoid(body):
	if body.collision_layer == 1:
		attacking = false
		avoid_remaining = avoid_duration