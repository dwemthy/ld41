extends Sprite

export(PackedScene) var next

var slot1
var slot2
var slot3
var slot4

var to_takeover

func prepare_takeover(other):
	to_takeover = other

func _ready():
	add_to_group("platform")
	slot1 = get_node("Slot1")
	slot2 = get_node("Slot2")
	slot3 = get_node("Slot3")
	slot4 = get_node("Slot4")
	refresh()

func refresh():
	if slot1 != null:
		get_tree().call_group("add_slot", "add_slot", "slot1")
	if slot2 != null:
		get_tree().call_group("add_slot", "add_slot", "slot2")
	if slot3 != null:
		get_tree().call_group("add_slot", "add_slot", "slot3")
	if slot4 != null:
		get_tree().call_group("add_slot", "add_slot", "slot4")
		
	if to_takeover != null:
		takeover(to_takeover)
		to_takeover = null
		
func takeover(other):
	var other1 = other.get_node("Slot1")
	var other2 = other.get_node("Slot2")
	var other3 = other.get_node("Slot3")
	var other4 = other.get_node("Slot4")
	
	if slot1 != null && other1 != null:
		var turret1 = other1.get_turret()
		other1.remove_child(turret1)
		slot1.set_turret(turret1)
	
	if slot2 != null && other2 != null:
		var turret2 = other2.get_turret()
		other2.remove_child(turret2)
		slot2.set_turret(turret2)
		
	if slot3 != null && other3 != null:
		var turret3 = other3.get_turret()
		other3.remove_child(turret3)
		slot3.set_turret(turret3)
		
	if slot4 != null && other4 != null:
		var turret4 = other4.get_turret()
		other4.remove_child(turret4)
		slot4.set_turret(turret4)
	
	other.get_parent().remove_child(other)
	other.queue_free()
	
	name = "Platform"