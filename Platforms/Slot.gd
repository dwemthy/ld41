extends Node2D

export var slot_name = "slot1"

var turret

func _ready():
	add_to_group(slot_name)
	
func set_turret(turret):
	if turret != null:
		self.turret = turret
		add_child(turret)
		get_tree().call_group(slot_name + "_watchers", "turret_set")
		get_tree().call_group(slot_name + "_watchers", "set_textures", turret.normal, turret.hover, turret.pressed)
		get_tree().call_group(slot_name + "_watchers", "set_upgrade_cost", turret.upgrade_cost())
	
func get_turret():
	return turret
	
func refresh():
	if turret != null:
		get_tree().call_group(slot_name + "_watchers", "set_upgrade_cost", turret.upgrade_cost())
	else:
		get_tree().call_group(slot_name + "_watchers", "set_upgrade_cost", -2)

func upgrade(upgrade_group):
	if turret != null && turret.nextLevel != null:
		var upgraded = turret.nextLevel.instance()
		upgraded.position = Vector2()
		
		get_tree().call_group("dust_spent", "spend_dust", turret.upgrade_cost())
		
		turret.queue_free()
		set_turret(upgraded)