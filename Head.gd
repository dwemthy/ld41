extends Area2D

export var gore_damage = 1
export var gore_refractory = 1.0
export var charge_multiplier = 2

var targets = []
var since_gore = 0
var charging = false
var turn = 0.5

func _ready():
	since_gore = gore_refractory
	connect("body_entered", self, "acquire")
	connect("body_exited", self, "unacquire")
	
func _physics_process(delta):
	if is_refractory():
		since_gore += delta
	elif targets.size() > 0 && targets[0] != null:
		var target = targets[0]
		var damage = gore_damage
		if charging:
			damage *= charge_multiplier
			
		target.emit_signal("hit", damage)
		since_gore = 0
	
func is_refractory():
	return since_gore < gore_refractory
	
func acquire(body):
	targets.append(body)
	
func set_charging(to_set):
	charging = to_set
	
func unacquire(body):
	var to_remove = -1
	for i in range(0, targets.size()):
		if targets[i] == body:
			to_remove = i
			break
	
	if to_remove >= 0:
		targets.remove(to_remove)

func turn(turn_wise):
	var rotate_to = turn * turn_wise
	rotate(rotate_to - rotation)