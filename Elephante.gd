extends KinematicBody2D

signal collect

export var maxHealth = 10
export var health = 10
export var speed = 20
export var rotationalSpeed = 0.3
export var stamina = 100
export var maxStamina = 100
export var staminaSpend = 2
export var staminaRegen = 0.5

var dustAmount = 0
var angle
var direction = Vector2(0,1)
var head

class HPUpgrade:
	var to_add
	var cost
	var next
	var level
	
class TuskUpgrade:
	var damage_add
	var cost
	var next
	var level
	
class LegUpgrade:
	var speed_add
	var stamina_spend_minus
	var cost
	var next
	var level
	
class SlotUpgrade:
	var level
	var cost
	var next
	
var hp_upgrade
var tusk_upgrade
var leg_upgrade
var slot_upgrade

func _ready():
	angle = rotation;
	connect("collect", self, "collect")
	add_to_group("dust_spent")
	add_to_group("tusk")
	add_to_group("leg")
	add_to_group("body")
	add_to_group("slot")
	
	head = get_node("Head")
	
	create_upgrades()
	get_tree().call_group("leg_watchers", "set_upgrade_cost", leg_upgrade.cost)
	get_tree().call_group("leg_watchers", "update_level", 0)
	get_tree().call_group("tusk_watchers", "set_upgrade_cost", tusk_upgrade.cost)
	get_tree().call_group("tusk_watchers", "update_level", 0)
	get_tree().call_group("body_watchers", "set_upgrade_cost", hp_upgrade.cost)
	get_tree().call_group("body_watchers", "update_level", 0)
	get_tree().call_group("slot_watchers", "set_upgrade_cost", slot_upgrade.cost)
	get_tree().call_group("slot_watchers", "update_level", 0)
	
func _physics_process(delta):
	# rotate with arrow keys
	head.turn(0)
	
	if Input.is_action_pressed("right"):
		head.turn(1)
		angle += rotationalSpeed
	
	if Input.is_action_pressed("left"):
		head.turn(-1)
		angle -= rotationalSpeed
	
	direction = Vector2(cos(angle), sin(angle))
	rotate(atan2(direction.x, -direction.y) - rotation)
	
	get_tree().call_group("stamina_monitor", "set_stamina", stamina)
	get_tree().call_group("stamina_monitor", "set_stamina_max", maxStamina)
	get_tree().call_group("health_monitor", "set_health", health)
	get_tree().call_group("health_monitor", "set_health_max", maxHealth)
	
	#Stop to gore shit!
	if head.is_refractory():
		return
		
	# move forward constantly
	var speed_to_use = speed
	if Input.is_action_pressed("space"):
		if stamina > staminaSpend:
			stamina -= staminaSpend
			speed_to_use += speed
			head.set_charging(true)
		else:
			head.set_charging(false)
	else:
		stamina += staminaRegen
		if stamina > maxStamina:
			stamina = maxStamina
			
		if Input.is_action_pressed("down"):
			speed_to_use = speed * 0.5
			
		head.set_charging(false)
		
	get_tree().call_group("stamina_monitor", "set_stamina", stamina)
	get_tree().call_group("stamina_monitor", "set_stamina_max", maxStamina)
	move_and_collide(direction * speed_to_use * delta)
	
func collect(amount):
	dustAmount += amount
	get_tree().call_group("dust_watchers", "update_dust", dustAmount)
	
func spend_dust(amount):
	dustAmount -= amount
	get_tree().call_group("dust_watchers", "update_dust", dustAmount)
	
func damage(amount):
	health -= amount
	if health <= 0:
		get_tree().change_scene("res://Lose.tscn")

func upgrade(upgrade_group):
	match upgrade_group:
		"tusk":
			set_tusk_upgrade(tusk_upgrade)
			
		"leg":
			set_leg_upgrade(leg_upgrade)
			
		"body":
			set_hp_upgrade(hp_upgrade)
			
		"slot":
			set_slot_upgrade(slot_upgrade)
		
func create_upgrades():
	hp_upgrade = HPUpgrade.new()
	hp_upgrade.level = 1
	hp_upgrade.to_add = 50
	hp_upgrade.cost = 50
	
	hp_upgrade.next = HPUpgrade.new()
	hp_upgrade.next.level = 2
	hp_upgrade.next.to_add = 50
	hp_upgrade.next.cost = 50
	
	hp_upgrade.next.next = HPUpgrade.new()
	hp_upgrade.next.level = 3
	hp_upgrade.next.next.to_add = 50
	hp_upgrade.next.next.cost = 50
	
	tusk_upgrade = TuskUpgrade.new()
	tusk_upgrade.level = 1
	tusk_upgrade.damage_add = 2
	tusk_upgrade.cost = 50
	
	tusk_upgrade.next = TuskUpgrade.new()
	tusk_upgrade.next.level = 2
	tusk_upgrade.next.cost = 75
	tusk_upgrade.next.damage_add = 3
	
	tusk_upgrade.next.next = TuskUpgrade.new()
	tusk_upgrade.next.next.level = 3
	tusk_upgrade.next.next.cost = 100
	tusk_upgrade.next.next.damage_add = 5
	
	leg_upgrade = LegUpgrade.new()
	leg_upgrade.level = 1
	leg_upgrade.speed_add = 50
	leg_upgrade.cost = 30
	leg_upgrade.stamina_spend_minus = 0
	
	leg_upgrade.next = LegUpgrade.new()
	leg_upgrade.next.level = 2
	leg_upgrade.next.speed_add = 50
	leg_upgrade.next.stamina_spend_minus = 0.25
	leg_upgrade.next.cost = 50
	
	leg_upgrade.next.next = LegUpgrade.new()
	leg_upgrade.next.next.level = 3
	leg_upgrade.next.next.speed_add = 50
	leg_upgrade.next.next.stamina_spend_minus = 0.5
	leg_upgrade.next.next.cost = 75
	
	slot_upgrade = SlotUpgrade.new()
	slot_upgrade.level = 1
	slot_upgrade.cost = 100
	
	slot_upgrade.next = SlotUpgrade.new()
	slot_upgrade.next.level = 2
	slot_upgrade.next.cost = 200
	
	slot_upgrade.next.next = SlotUpgrade.new()
	slot_upgrade.next.next.level = 3
	slot_upgrade.next.next.cost = 300
	
func set_leg_upgrade(leg):
	if leg != null:
		spend_dust(leg.cost)
		leg_upgrade = leg.next
		speed += leg.speed_add
		staminaSpend -= leg.stamina_spend_minus
		
		if leg_upgrade == null:
			get_tree().call_group("leg_watchers", "set_upgrade_cost", -1)
		else:
			get_tree().call_group("leg_watchers", "set_upgrade_cost", leg_upgrade.cost)
			
		get_tree().call_group("leg_watchers", "update_level", leg.level)
	else:
		get_tree().call_group("leg_watchers", "set_upgrade_cost", -1)
	
func set_tusk_upgrade(tusk):
	if tusk != null:
		spend_dust(tusk.cost)
		tusk_upgrade = tusk.next
		head.gore_damage += tusk.damage_add
		
		if tusk_upgrade == null:
			get_tree().call_group("tusk_watchers", "set_upgrade_cost", -1)
		else:
			get_tree().call_group("tusl_watchers", "set_upgrade_cost", tusk_upgrade.cost)
			
		get_tree().call_group("tusk_watchers", "update_level", tusk.level)
	else:
		get_tree().call_group("tusk_watchers", "set_upgrade_cost", -1)
	
func set_hp_upgrade(hp):
	if hp != null:
		spend_dust(hp.cost)
		hp_upgrade = hp.next
		health += hp.to_add
		maxHealth += hp.to_add
		
		if hp_upgrade == null:
			get_tree().call_group("body_watchers", "set_upgrade_cost", -1)
		else:
			get_tree().call_group("body_watchers", "set_upgrade_cost", hp_upgrade.cost)
			
		get_tree().call_group("body_watchers", "update_level", hp.level)
	else:
		get_tree().call_group("body_watchers", "set_upgrade_cost", -1)
		
func set_slot_upgrade(slot):
	if slot != null:
		spend_dust(slot.cost)
		slot_upgrade = slot.next
		
		add_slot()
		
		if slot_upgrade == null:
			get_tree().call_group("slot_watchers", "set_upgrade_cost", -1)
		else:
			get_tree().call_group("slot_watchers", "set_upgrade_cost", slot_upgrade.cost)
			
		get_tree().call_group("slot_watchers", "update_level", slot.level)
	else:
		get_tree().call_group("slot_watchers", "set_upgrade_cost", -1)
		
func refresh():
	get_tree().call_group("leg_watchers", "set_upgrade_cost", leg_upgrade.cost)
	get_tree().call_group("leg_watchers", "update_level", leg_upgrade.level - 1)
	get_tree().call_group("tusk_watchers", "set_upgrade_cost", tusk_upgrade.cost)
	get_tree().call_group("tusk_watchers", "update_level", tusk_upgrade.level - 1)
	get_tree().call_group("body_watchers", "set_upgrade_cost", hp_upgrade.cost)
	get_tree().call_group("body_watchers", "update_level", hp_upgrade.level - 1)
	get_tree().call_group("slot_watchers", "set_upgrade_cost", slot_upgrade.cost)
	get_tree().call_group("slot_watchers", "update_level", slot_upgrade.level - 1)
	
func add_slot():
	var platform = get_node("Platform")
		
	if platform != null && platform.next != null:
		var new_platform = platform.next.instance()
		new_platform.prepare_takeover(platform)
		add_child(new_platform)