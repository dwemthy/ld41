extends KinematicBody2D


export(PackedScene) var Bird
export var bird_max = 5
export var spawn_cooldown = 0.5
export var win_condition = false

signal hit
signal bird_died

export(PackedScene) var Dust
export var dust_drop = 1
export var health = 1

var birds = []
var since_spawn

func _ready():
	since_spawn = spawn_cooldown
	connect("hit", self, "damage")
	
func _physics_process(delta):
	since_spawn += delta
	if since_spawn >= spawn_cooldown && birds.size() < bird_max:
		spawn_bird()

func spawn_bird():
	if Bird != null:
		var bird = Bird.instance()
		bird.set_house(self)
		bird.position = to_global(Vector2())
		get_parent().add_child(bird)
		birds.append(bird)
		since_spawn = 0
		
func bird_died(bird):
	var to_remove = -1
	for i in range(0, birds.size()):
		if birds[i] == bird:
			to_remove = i
			break
			
	if to_remove > -1:
		birds.remove(to_remove)
		
func damage(amount):
	health -= amount
	if health <= 0:
		for i in range(0, birds.size()):
			birds[i].set_house(null)
		spawn_dust()
		queue_free()
		if win_condition:
			get_tree().change_scene("res://Win.tscn")
		
func spawn_dust():
	for i in range(0, dust_drop):
		var dust = Dust.instance()
		dust.position = position + Vector2(i * 5, i * 3)
		get_parent().add_child(dust)