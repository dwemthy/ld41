extends Area2D

export var damage = 5
export var duration = 0.5

var elapsed

func _ready():
	elapsed = 0
	connect("body_entered", self, "hit")

func hit(body):
	body.damage(damage)
	
func _physics_process(delta):
	elapsed += delta
	if elapsed > duration:
		queue_free()