extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	connect("button_down", self, "start")

func start():
	get_tree().change_scene("res://World.tscn")